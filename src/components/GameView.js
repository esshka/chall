import React from 'react';
import { observer } from 'mobx-react';
import CellView from './CellView';
import css from './GameView.scss';

const GameView = observer(({ store }) => (
  <div>
    <div className={css.game} style={{ width: store.WIDTH * store.CELL_SIZE }}>
      {store.cells.map((r, idx) => (
        <div key={idx} className={css.row}>
          {r.map(c => (
            <CellView key={c.cellIndex} cell={c} store={store} />
          ))}
        </div>
      ))}
    </div>
    <div className={css.actions}>
      <button onClick={store.runGame}>run</button>
      <button onClick={store.resetGame}>reset</button>
    </div>
  </div>
));

export default GameView;
