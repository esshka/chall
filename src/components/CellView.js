import React from 'react';
import { observer } from 'mobx-react';
import css from './CellView.scss';
import cn from 'classnames';

const CellView = ({ cell, store }) => {
  return (
    <div
      onClick={cell.toggleRock}
      className={cn(css.cell, {
        [css.rock]: cell.isRock,
        [css.water]: cell.isWater
      })}
      style={{ width: store.CELL_SIZE, height: store.CELL_SIZE }}
    >
      {cell.index}
    </div>
  );
};

export default observer(CellView);
