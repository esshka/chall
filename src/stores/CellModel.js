import { observable, action, computed } from 'mobx';

export default class CellModel {
  @observable
  isRock = false;

  @observable
  isWater = false;

  constructor({ cellIndex, rowIndex, store }) {
    this.cellIndex = cellIndex;
    this.rowIndex = rowIndex;
    this.store = store;
  }

  @action.bound
  toggleRock() {
    this.isRock = !this.isRock;
  }

  @action.bound
  clear() {
    this.isRock = false;
    this.isWater = false;
  }

  @action.bound
  run() {
    if (this.canBeWater) {
      this.isWater = true;
    }
  }

  @computed
  get canBeWater() {
    if (this.isRock) return false;

    if (this.isInsidePit) return true;

    return false;
  }

  @computed
  get isInsidePit() {
    if (this.isRock) return false;

    if (
      this.leftCells.some(c => c.isRock) &&
      this.rightCells.some(c => c.isRock)
    ) {
      return true;
    }

    return false;
  }

  @computed
  get leftCells() {
    return this.store.getCellsFromTheLeftOfCell(this);
  }

  @computed
  get rightCells() {
    return this.store.getCellsFromTheRightOfCell(this);
  }
}
