import { observable, action } from 'mobx';
import { forEachRight } from 'lodash';
import CellModel from './CellModel';

const createEmptySizedArray = size => Array.from(Array(size).keys());

export default class RootStore {
  WIDTH = 8;
  HEIGHT = 5;
  CELL_SIZE = 40;

  cells = [];

  constructor() {
    this.init();
  }

  init() {
    const rows = createEmptySizedArray(this.HEIGHT);
    const cells = rows.map(rowIndex =>
      createEmptySizedArray(this.WIDTH).map(
        cellIndex => new CellModel({ rowIndex, cellIndex, store: this })
      )
    );
    this.cells = cells;
  }

  getCell = (rowIndex, cellIndex) => {
    if (!this.cells[rowIndex]) return undefined;

    return this.cells[rowIndex][cellIndex];
  };

  getCellsFromTheLeftOfCell = cell => {
    return this.cells[cell.rowIndex].filter(c => c.cellIndex < cell.cellIndex);
  };

  getCellsFromTheRightOfCell = cell => {
    return this.cells[cell.rowIndex].filter(c => c.cellIndex > cell.cellIndex);
  };

  @action.bound
  runGame() {
    this.eachCell(c => c.run());
  }

  @action.bound
  resetGame() {
    this.eachCell(c => c.clear());
  }

  @action.bound
  eachCell(func) {
    forEachRight(this.cells, r => {
      r.forEach(c => func(c));
    });
  }
}
