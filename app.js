import React from 'react';
import { render } from 'react-dom';
import RootStore from './src/stores/RootStore';
import GameView from './src/components/GameView';

const store = new RootStore();

window.store = store;

render(
  <div>
    <GameView store={store} />
  </div>,
  document.getElementById('root')
);
